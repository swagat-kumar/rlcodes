#include<iostream>
#include<opencv2/opencv.hpp>
#include<opencv2/core/core.hpp>
#include<vector>
#include<ctime>
#include<cstdlib>

using namespace std;

          
//#define INIT
#define RANDGEN
#define rowRANGE


int main()
{

#ifdef INIT
  int a[9] = {1, 2, 0, 0, 1, 2, 0, 0, 1};
  cv::Mat A = cv::Mat(1, 9, CV_32SC1, a);

  for(int i = 0; i < 9; i++)
    cout << A.at<int>(i) << endl;


  cv::Mat loc;

  for(int i = 0; i < A.cols; i++)
  {
    if(A.at<int>(i) > 0)
      loc.push_back(i);
  }

  for(int i = 0; i < loc.cols; i++)
    cout << loc.at<int>(i) << "\t";
  cout << endl;

#endif

#ifdef RANDGEN

  cv::RNG rng;
  rng = cv::RNG(time(NULL));

  for(int i = 0; i < 10; i++)
  {
    int num = rng.uniform(0,10);
    cout << num << "\t";
  }
  cout << endl;


#endif

#ifdef rowRANGE

 int a[9] = {1, 2, 0, 0, 1, 2, 0, 0, 1};
 cv::Mat A = cv::Mat(3, 3, CV_32SC1, a); 

 cv::Mat B = A.rowRange(1,3);

 cout << "A = " << A << endl;
 cout << "B = " << B << endl;



#endif


  return 0;


}
