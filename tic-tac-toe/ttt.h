#ifndef TTT_H
#define TTT_H


#include <cmath>
#include <fstream>
#include <algorithm>
#include <opencv2/opencv.hpp>
#include <omp.h>



namespace ttt
{

class ttt_game
{
private:
    int NS, D;
    cv::Mat current_state;   // State of the board
    cv::Mat next_state;


    cv::Mat stateTable, valueTable;


    double current_value, next_value;


    int winner, winIndex;

    int player;
    const int playerA = 1;
    const int playerB = 2;
    const int noplayer = 0;

    cv::RNG rng;

public:
    ttt_game();
    void display_board_current();
    void display_board_next();
    void reset_board();
    int get_board_dimension();

    bool make_move(cv::Mat &statevec);
    void make_move_random();
    void make_move_selfplay(int pl, double alpha = 0.1);
    void make_move_robot(int selfPlayer, double alpha);
    void make_move_human(int dumbPlayer);

    double get_value(int player);
    void update_valueTable(bool drawF);
    int check_winner(bool &drawFlag);



    int find_state_index(const cv::Mat &statevec);

    bool check_gameover(const cv::Mat &statevec); //could be removed


    void find_empty_cell_locations(const cv::Mat &statevec, cv::Mat& eloc, cv::Mat& neloc);
    double boltzman_pdf_policy_selection(int pl, cv::Mat &child_idx_array, int &matching_state_idx);
    int compute_children(cv::Mat& child_idx_array);

    void get_next_state(cv::Mat& statevec);
    void set_next_state(const cv::Mat& statevec);


    void display_stateValueTable();
    int read_stateValueTable(const char*, int ndata);
    int print_stateValueTable(const char* filename);
};


}

#endif // TTT_H
