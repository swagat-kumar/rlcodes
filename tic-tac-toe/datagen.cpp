/* Generating all possible states for a tic-tac-toe game of any
 * dimension
 *
 * Author: Swagat Kumar
 * Email: swagat.kumar@gmail.com
 * License: GPL V 3.0 or above
 * Date: August 28, 2016
 * ==============================================*/

#include <iostream>
#include <fstream>
#include <cmath>

using namespace std;

#define N 9        // vector dimension
#define M 3        // number of choices for each element

#define MAX 20000

int Data[MAX][N];

std::ofstream f1("dataset3.txt");
//-------------------------------------------

bool find_match(int state[N], int rowcnt)
{
#pragma openmp parallel for
  for(int q = 0; q < rowcnt; q++)
  {
    int simcnt = 0;
#pragma openmp parallel for
    for(int r = 0; r < N; r++)
    {
      if(Data[q][r] == state[r])
        simcnt++;
    }

    if(simcnt == N)
      return true;
  }

  return false;
}

//------------------------------------

void add_row(int state[N], int rowcnt)
{

#pragma openmp parallel for
  for(int r = 0; r < N; r++)
    Data[rowcnt][r] = state[r];

}

//-----------------------------------------

void genstate(int state[], int k, int &rowcnt)
{

#pragma openmp parallel for
  bool mFlag = false;
  for(int l = k; l < N; l++)
  {
#pragma openmp parallel for
    for(int j = 0; j < M; j++)
    {
      state[k] = j;

      mFlag = find_match(state, rowcnt);
      if(mFlag == false)
      {

        if(rowcnt%100 == 0)
          cout << "rowcnt = " << rowcnt << endl;

        add_row(state, rowcnt);
        rowcnt++;
        for(int p = 0; p < N; p++)
          f1 << state[p] << "\t";
        f1 << endl;
      }
     
      genstate(state, k+1,rowcnt);
    }           
  }
}

//-------------------------

int main()
{
  int state[N];
  for(int i = 0; i < N; i++)
    state[i] = 0;

  int rowcnt = 0;

  genstate(state, 0, rowcnt);


  f1.close();

  return 0;
}
