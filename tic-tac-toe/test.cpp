#include <iostream>
#include <ttt.h>
#include <cmath>
#include <fstream>

using namespace std;
using namespace ttt;



//#define DATAGEN       // Generate training data

//#define PLAY_DUMB     // Play against a dumb player
//#define SELF_PLAY     // Computer plays against itself
#define TEST_PLAY
//#define UPDATE_VALUE

#define NMAX 200000
#define N 9
#define DMAX 8533  // data  points in the file

// Player 1 is the robot
// Player 2 is the human (making random choice)

int main()
{
    // Initiate a Game
    ttt_game G;

#ifdef DATAGEN

    for(int itr = 0; itr < NMAX; itr++)
    {
        //start a new game
        G.reset_board();

        bool gameOver = false;
        int winner = 0;
        int cnt = 0;
        do
        {
            G.make_move_random();
            winner = G.check_winner(gameOver);

            //G.display_board_next();

            // update the value table
            G.update_valueTable(gameOver);

            if(winner > 0)
                break;

            cnt++;

        }while(!gameOver);

        cout << "Episode = " << itr << ": Winner = " << winner << ": Step count =  " << cnt << endl;

    }

    G.print_stateValueTable("trgdata.txt");

#endif

#ifdef UPDATE_VALUE

    cv::Mat statevec = cv::Mat::zeros(1, N, CV_32SC1);
    ifstream f1("trgdata3.txt");
    ofstream f2("dataset_aug31.txt");

    if(f1.good())
    {
        int a;
        int winner;
        bool drawFlag = false;
        double value = 0;
        for(int k = 0; k < DMAX; k++)
        {
            for(int j = 0; j < N; j++)
            {
                f1 >> a;
                f2 << a << "\t";
                statevec.at<int>(j) = a;
            }
            G.set_next_state(statevec);
            winner = G.check_winner(drawFlag);

            if(winner == 1) // Robot is winner
                value = 1.0;
            else if(winner == 2)
                value = 0.0;
            else if(drawFlag == true)
                value = 0.0;
            else if(winner == 0)
                value = 0.5;

            f2 << value << endl;
        }
    }
    else
    {
        cout << __FILE__ << ":" << __LINE__ << "File does not exist" << endl;
        exit(-1);
    }

    f1.close();
    f2.close();

#endif

// Play against dumb opponent
#ifdef PLAY_DUMB

    cout << "========================" << endl;
    cout << " Robot plays against a dumb player" << endl;
    cout << "===================================" << endl;


    G.read_stateValueTable("trgdata1.txt", DMAX);

    cout << "State table is read ..." << endl;

    for(int k = 0; k < NMAX; k++)
    {
        // start a new game
       G.reset_board();

       bool gameOver = false;
       int winner = 0;
       int cnt = 0;
       double alpha = 0.1;

       do
       {
           G.make_move_selfplay(2, alpha);
           winner = G.check_winner(gameOver);

           if(winner > 0)
               break;

           cnt++;
       }while(!gameOver);

       if((k%100) == 0)
           cout << "Episode = " << k << ": Winner = " << winner << " : step count = " << cnt << endl;
    }

    cout << "saving the new State Value Table" << endl;

    G.print_stateValueTable("trgdata2.txt");

#endif

    // Play against a human player
#ifdef TEST_PLAY

    G.read_stateValueTable("trgdata3.txt", DMAX);
    G.reset_board();

    cout << "Robot is player 1 with 'X' symbol" << endl;
    cout << "Human is player 2 with 'O' symbol" << endl;

    bool gameOver = false;
    int winner = 0;
    int cnt = 0;
    int player = 2;
    double alpha = 0.1;
    do
    {
        G.display_board_next();

        player = (player%2)?1:2;

        cout << "player = " << player << endl;

        if(player == 1) // Robot
            G.make_move_robot(1, alpha);
        else if(player == 2)
            G.make_move_human(2);

         //G.display_board_next();

        winner = G.check_winner(gameOver);

        if(winner > 0)
            break;

        cnt++;
        player++;
    }while(!gameOver);

    cout << "Winner = " << winner << ": step count = " << cnt << endl;

#endif

    // Computer Plays against itself.

#ifdef SELF_PLAY

    const int max_EPOCH 2000;
    const int max_ITERN 100;

    double alpha = 0.1;
    G.read_stateValueTable("trgdata2.txt", DMAX);


    ofstream f1("playerstat.txt");
    ofstream f2("gamevalue.txt");

    int game_cnt = 0;

    for(int epoch = 0; epoch < max_EPOCH; epoch++)
    {

        int winplayer1_cnt = 0;
        int winplayer2_cnt = 0;
        int nowinner_cnt = 0;
        double winvalue1 = 0.0;
        double winvalue2 = 0.0;
        double nowinvalue = 0.0;
        for(int k = 0; k < max_ITERN; k++)
        {

            G.reset_board();

            bool gameOver = false;
            int winner = 0;
            int cnt = 0;
            int player = 1;
            double sum_value1 = 0.0;
            double sum_value2 = 0.0;
            double sum_value3 = 0.0;
            double value;
            do
            {
                player = (player%2)?1:2;

                G.make_move_robot(player, alpha);

                winner = G.check_winner(gameOver);

                value = G.get_value(player);

                if(player == 1)
                    sum_value1 += value;
                else if(player == 2)
                    sum_value2 += value;
                else
                    sum_value3 += value;

                if(winner > 0)
                    break;

                player++;

                cnt++;
            }while(!gameOver);

            if(winner == 1)
            {
                winplayer1_cnt++;
                winvalue1 += sum_value1 / cnt;
            }
            else if(winner == 2)
            {
                winplayer2_cnt++;
                winvalue1 += sum_value2 / cnt;
            }
            else
            {
                nowinner_cnt++;
                nowinvalue += sum_value3 / cnt;
            }

            game_cnt++;

            f2 << game_cnt << "\t" << winner << "\t"
                  << sum_value1 / cnt << "\t"
                     << sum_value2 / cnt << "\t"
                     << sum_value3 / cnt << endl;

            //cout << "Episode = " << k << ": Winner = " << winner << " : Step Count = " << cnt << endl;

        } // iteration

        cout << winplayer1_cnt << "\t" << winplayer2_cnt << "\t"
             << nowinner_cnt << "\t" << winvalue1/max_ITERN << "\t"
             << winvalue2/max_ITERN << "\t" << nowinvalue / max_ITERN <<endl;

        f1 << winplayer1_cnt << "\t" << winplayer2_cnt << "\t"
             << nowinner_cnt << "\t" << winvalue1/max_ITERN << "\t"
             << winvalue2/max_ITERN << "\t" << nowinvalue / max_ITERN <<endl;


    }//epoch

    f1.close();
    f2.close();

    //save the final state value table
    G.print_stateValueTable("trgdata3.txt");

#endif


    return 0;
}

