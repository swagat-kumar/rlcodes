/* Ternary Search Tree for integers
 * Date: July 31, 2016; Sunday
 * Author: Swagat Kumar
 * Email: swagat.kumar@gmail.com
 * License: GPL V 2.0 or above
 *
 * Compile instruction:
 *
 * $ g++ tstn.cpp
 * $ ./a.out
 * $ dot -Tps tst_data.dot -o tst_data.ps
 * $ gv tst_data.ps (to See the tree)
 * -------------------------------------------------- */
#include <iostream>
#include <fstream>
#include <cstdlib>

using namespace std;

const int N = 3;


/*
 * NODE Declaration
 */

int node_count = 0;
struct Node
{
  int data;
  bool tFlag;
  int id;

  Node *left, *eq, *right;
};


void tst_print_dot(Node* node, FILE* stream);
void tst_print_dot_aux(Node* node, FILE* stream);
void tst_print_dot_null(int key, int nullcount, FILE* stream);
/*
 * Create a new ternary search tree node
 */

Node* newNode(int somedata[], int depth)
{
  Node* temp;
  
  if(depth < N)
  {
    temp = new Node;
    temp->data = somedata[depth];
    temp->id = node_count; 
    temp->tFlag = false;
    temp->left = temp->eq = temp->right = NULL;
    node_count++;
  }
  else
    temp = NULL;

  return temp;
}

/*
 * Insert a new node into the tree
 */

void insertTST(Node** root, int somedata[], int depth)
{
  if(!(*root))
    *root = newNode(somedata, depth);

  if(depth < N)
  {
    if((*root)->data == somedata[depth])
    {
      if(somedata[depth+1] == 0)
        insertTST(&((*root)->eq),somedata, depth+1);
      else if(somedata[depth+1] == 1)
        insertTST(&((*root)->left),somedata, depth+1);
      else if(somedata[depth+1] == 2)
        insertTST(&((*root)->right),somedata, depth+1);
    }
    else
    {
      if(somedata[depth] == 0)
        insertTST(&((*root)->eq),somedata, depth);
      else if(somedata[depth] == 1)
        insertTST(&((*root)->left),somedata, depth);
      else if(somedata[depth] == 2)
        insertTST(&((*root)->right),somedata, depth);
    }
  }
}

/*
 * Search ternary search tree
 */

bool searchTST(Node* root, int somedata[], int depth, int &cnt )
{
  bool found = false;
  if(!root)
    return 0;
  else
  {
    if(depth < N)
    {
      if(root->data == somedata[depth])
      {
        cnt++;
        if(somedata[depth+1] == 0)
          found = searchTST(root->eq, somedata, depth+1, cnt);
        else if(somedata[depth+1] == 1)
          found = searchTST(root->left, somedata, depth+1, cnt); 
        else if(somedata[depth+1] == 2)
          found = searchTST(root->right, somedata, depth+1, cnt); 
      }
      else
      {
        if (somedata[depth] == 0)
          found = searchTST(root->eq, somedata, depth, cnt);
        else if(somedata[depth] == 1)
          found = searchTST(root->left, somedata, depth, cnt);
        else if(somedata[depth] == 2)
          found = searchTST(root->right, somedata, depth, cnt);
      }
    }

    if(cnt == N)
    {
      return true;
    }
    else
    {
      if (found == true)
        return 1;
      else
        return 0;
    }
  }
}


/* 
 * Main Function
 */


int main()
{
  Node *root = NULL;
  int data1[N] = {0, 0, 0};
  int data2[N] = {1, 0, 0}; 
  int data3[N] = {0, 1, 2};
  int data4[N] = {1, 0, 2};
  int data5[N] = {2, 1, 0};

  // Insert function
  insertTST(&root, data1, 0); 
  insertTST(&root, data2, 0);
  insertTST(&root, data3, 0);
  insertTST(&root, data4, 0);
  insertTST(&root, data5, 0);

  cout << "-----------------------" << endl;


    FILE *fp = fopen("tst_data.dot", "w");
    tst_print_dot(root, fp);
    fclose(fp);

  

  cout << "-----------------------" << endl;

  int sdata[N] = {2, 2, 0};

  int cnt = 0;
  if(searchTST(root, sdata, 0, cnt))
    cout << "Match is found." << endl;
  else
    cout << "Match is not found." << endl;



  return 0;
}

//--------------------------------
      

void tst_print_dot(Node* node, FILE* stream)
{
  fprintf(stream, "digraph TST {\n");
  fprintf(stream, "  node [fontname=\"Arial\"];\n");

  if(!node)
    fprintf(stream, "\n");
  //else if (!node->left && !node->right && !node->eq)
  //  fprintf(stream, "    %d;\n", node->id);
  else
    tst_print_dot_aux(node, stream);
  fprintf(stream, "}\n");
}

//-----------------------------
void tst_print_dot_aux(Node* node, FILE* stream)
{
    static int nullcount = 0;

    if (node->left)
    {
        fprintf(stream, "%d [label = \"%d\"];\n", node->id, node->data);
        fprintf(stream, "%d [label = \"%d\"];\n", node->left->id, node->left->data);
        fprintf(stream, "    %d -> %d;\n", node->id,  node->left->id);
        tst_print_dot_aux(node->left, stream);
    }
    else
        tst_print_dot_null(node->id, nullcount++, stream);

    if(node->eq)
    {                                                     
      fprintf(stream, "%d [label = \"%d\"];\n", node->id, node->data);
      fprintf(stream, "%d [label = \"%d\"];\n", node->eq->id, node->eq->data);
      fprintf(stream, "    %d -> %d;\n", node->id, node->eq->id);
      tst_print_dot_aux(node->eq, stream);
    }
    else
        tst_print_dot_null(node->id, nullcount++, stream);

    if (node->right)
    {
        fprintf(stream, "%d [label = \"%d\"];\n", node->id, node->data);
        fprintf(stream, "%d [label = \"%d\"];\n", node->right->id, node->right->data);
        fprintf(stream, "    %d -> %d;\n", node->id, node->right->id);
        tst_print_dot_aux(node->right, stream);
    }
    else
        tst_print_dot_null(node->id, nullcount++, stream);



}
//-----------------------------------------
 void tst_print_dot_null(int key, int nullcount, FILE* stream)
{
    fprintf(stream, "    null%d [shape=point];\n", nullcount);
    fprintf(stream, "    %d -> null%d;\n", key, nullcount);
}



