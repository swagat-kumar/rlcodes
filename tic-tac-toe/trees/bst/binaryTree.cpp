/*
 * Binary Search Tree
 */

#include <iostream>
#include <cstdlib>

using namespace std;

#define N 9

/*
 * Node Declaration
 */ 

struct node
{
  int data[N];
  int value;

  struct node *left;
  struct node *right;
}*root;


/*
 * Class Declaration
 */

class BST
{
  private:
    int depth;
    int nodeCount;

  public:
    void find(int, node **, node **);    
    void insert(int);
    void del(int);
    void case_a(node *,node *);
    void case_b(node *,node *);
    void case_c(node *,node *);
    void preorder(node *);
    void inorder(node *);
    void postorder(node *);
    void display(node *, int);
    BST()
    {
      root = NULL;
    }
};

/*
 * Find Element in the tree
 */

void BST::find(int somedata[], node **par, node **loc)
{
  node *ptr, *ptrsave;

  if(root == NULL)
  {
    *loc = NULL;
    *par = NULL;
    return;
  }
  if(
