#include <iostream>
#include <cstdio>

using namespace std;

#define N 5

struct tree
{
    tree *l, *r;
    int data;
}*root = NULL, *p = NULL, *np = NULL, *q;

//-------------------------------------------------------

void insertArray(int data[])
{
  int value,c = 0;   
  while (c < N)
  {
    if (root == NULL)
    {
      root = new tree;
      root->data = data[c];
      root->r=NULL;
      root->l=NULL;
    }
    else
    {
      p = root;
      value = data[c];
      while(true)
      {
        if (value < p->data)
        {
          if (p->l == NULL)
          {
            p->l = new tree;
            p = p->l;
            p->data = value;
            p->l = NULL;
            p->r = NULL;
            break;
          }
          else if (p->l != NULL)
          {
            p = p->l;
          }
        }
        else if (value > p->data)
        {
          if (p->r == NULL)
          {
            p->r = new tree;
            p = p->r;
            p->data = value;
            p->l = NULL;
            p->r = NULL;
            break;
          }
          else if (p->r != NULL)
          {
            p = p->r;
          }
        }
      }
    }
    c++;
  }
}
//-----------------------------------------

