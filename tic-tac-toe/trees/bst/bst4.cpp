#include <cstdio>
#include <cstring>
#include <cstdlib>

using namespace std;

typedef struct tree
{
    int data;
    struct tree *left;
    struct tree *right;
}tree;

int AddToArray(tree *node, int arr[], int i);
tree *CreateNode(int data);
tree *Insert(tree *node, int data);
void PrintPreorder(tree *node);
int count(tree *node);
int compare(const void * a, const void * b);

void bst_print_dot(tree *node, FILE* stream);
void bst_print_dot_aux(tree* node, FILE* stream);
void bst_print_dot_null(int key, int nullcount, FILE* stream);
//-------------------------------------------------------------------------------------------------
int main()
{
    int i;
    int size;
    int *arr=NULL;
    tree *root=NULL;

    printf("***TEST PROGRAM***\n");
    root = Insert(root, 4);
    root = Insert(root, 3);
    root = Insert(root, 5);
    root = Insert(root, 10);
    root = Insert (root, 8);
    root = Insert (root, 7);
    root = Insert (root, 2);
    root = Insert (root, 3);
    size = count(root);

    printf("\n***BINARY TREE (PREORDER)***\n");
    PrintPreorder(root);
    printf("\nThe binary tree countain %d nodes", size);

    printf("\n\n***ARRAY***\n");
    arr = new int[size];
    //arr = calloc(size, sizeof(int));
    AddToArray(root, arr, 0);
    qsort(arr,size,sizeof(int),compare);

    for (i=0; i<size; i++)
    {
        printf("arr[%d]: %d\n", i, arr[i]);
    }

    FILE *fp = fopen("bst_data.dot", "w");
    bst_print_dot(root, fp);
    fclose(fp);
}
//--------------------------------------------------------

int compare(const void * a, const void * b)
{
   return ( *(int*)a - *(int*)b );
}

//--------------------------------------------------------

int AddToArray(tree *node, int arr[], int i)
{
     if(node == NULL)
          return i;


     arr[i] = node->data;
     i++;
     if(node->left != NULL)
          AddToArray(node->left, arr, i);
     if(node->right != NULL)
          AddToArray(node->right, arr, i);


     arr[i] = node->data;
     i++;

}

//-------------------------------------------------------------

tree *CreateNode(int data)
{
  tree *node = new tree; //(tree *)malloc(sizeof(tree));
  node -> data = data;
  node -> left = node -> right = NULL;
  return node;
}

//-----------------------------------------------

tree *Insert(tree *node, int data)
{
  if(node==NULL)
  {
    tree *temp;
    temp = CreateNode(data);
    return temp;
  }

  if(data >(node->data))
  {
    node->right = Insert(node->right,data);
  }

  else if(data < (node->data))
  {
    node->left = Insert(node->left,data);
  }

  /* Else there is nothing to do as the data is already in the tree. */
  return node;
}

//---------------------------------------------------------

void PrintPreorder(tree *node)
{
  if(node==NULL)
  {
    return;
  }
  printf("%d ",node->data);
  PrintPreorder(node->left);
  PrintPreorder(node->right);
}

//--------------------------------------------

int count(tree *node)
{
    int c = 1;

    if (node == NULL)
        return 0;
    else
    {
        c += count(node->left);
        c += count(node->right);
        return c;
     }
}
//------------------------------

void bst_print_dot(tree *node, FILE* stream)
{
  fprintf(stream, "digraph BST {\n");
  fprintf(stream, "  node [fontname=\"Arial\"];\n");

  if(!node)
    fprintf(stream, "\n");
  else if (!node->left && !node->right)
    fprintf(stream, "    %d;\n", node->data);
  else
    bst_print_dot_aux(node, stream);
  fprintf(stream, "}\n");
}

//-----------------------------
void bst_print_dot_aux(tree* node, FILE* stream)
{
    static int nullcount = 0;

    if (node->left)
    {
        fprintf(stream, "    %d -> %d;\n", node->data, node->left->data);
        bst_print_dot_aux(node->left, stream);
    }
    else
        bst_print_dot_null(node->data, nullcount++, stream);

    if (node->right)
    {
        fprintf(stream, "    %d -> %d;\n", node->data, node->right->data);
        bst_print_dot_aux(node->right, stream);
    }
    else
        bst_print_dot_null(node->data, nullcount++, stream);
}
//-----------------------------------------
 void bst_print_dot_null(int key, int nullcount, FILE* stream)
{
    fprintf(stream, "    null%d [shape=point];\n", nullcount);
    fprintf(stream, "    %d -> null%d;\n", key, nullcount);
}

