#include<ttt.h>
#include <opencv2/opencv.hpp>
#include<opencv2/core/core.hpp>
#include<ctime>
#include<vector>

using namespace std;
using namespace ttt;

//#define MVROB_DEBUG
//#define MVSP_DEBUG

//#define CPP_OMP_DEF

ttt_game::ttt_game()
{
    D = 3;
    NS = D * D;


    current_state = cv::Mat::zeros(1, NS, CV_32SC1);
    next_state = cv::Mat::zeros(1, NS, CV_32SC1);

    stateTable = cv::Mat::zeros(1, NS, CV_32SC1);

    //there are two values, one for each player
    valueTable = cv::Mat::zeros(1,2, CV_64F);


#ifdef DEBUG
    cout << __LINE__ << ":size of state = " << next_state.rows<< "x" << next_state.cols << endl;
    getchar();
#endif

    rng = cv::RNG(time(NULL));

    player = 1;

    cout << "A Tic-Tac-Toe Game Instance is created." << endl;

}

//============================
// Displays the state of the board
//=========================================
void ttt_game::display_board_current()
{
    cv::Mat statmat = cv::Mat::zeros(D, D, CV_32SC1);

    cout << "--------------------------------" << endl;
    int k = 0;
    for(int i = 0; i < D; i++)
    {
      for(int j = 0; j < D; j++)
       {
          statmat.at<int>(i,j) = current_state.at<int>(k);
         if(statmat.at<int>(i,j) == playerA)
           cout << 'X' << "\t";
         else if(statmat.at<int>(i,j) == playerB)
           cout << 'O' << "\t";
         else
           cout << "--" << "\t";

         k++;
       }
      cout << endl;
    }
    cout << "-----------------------------------" << endl;
}

//============
// Displays the new state after the movement
void ttt_game::display_board_next()
{
    cv::Mat statmat = cv::Mat::zeros(D, D, CV_32SC1);

    cout << "--------------------------------" << endl;
    int k = 0;
    for(int i = 0; i < D; i++)
    {
      for(int j = 0; j < D; j++)
       {
          statmat.at<int>(i,j) = next_state.at<int>(k);
         if(statmat.at<int>(i,j) == playerA)
           cout << 'X' << "\t";
         else if(statmat.at<int>(i,j) == playerB)
           cout << 'O' << "\t";
         else
           cout << "--" << "\t";

         k++;
       }
      cout << endl;
    }
    cout << "-----------------------------------" << endl;
}
//===============================================
// Finds the winner from the state of the board
// Input: next_state
// Output: 0, 1 or 2
//-------------------------------------------

int ttt_game::check_winner(bool& drawFlag)
{

    cv::Mat sqmat = cv::Mat::zeros(D, D, CV_32SC1);

    int k = 0;
    for(int i = 0; i < D; i++)
    {
        for(int j = 0; j < D; j++)
        {
            sqmat.at<int>(i,j) = next_state.at<int>(k);
            k++;
        }
    }

  // rowsum, colsum, diasum1 and diasum2 will be equal D if all of
  // the cells in rows, columns or diagonals are filled with same
  // kind of states.


  int rowsum[D];
  int colsum[D];
  int diasum1 = 0;
  int diasum2 = 0;
  int row_label[D];
  int col_label[D];
  int dia_label1;
  int dia_label2;


  for(int i = 0; i < D; i++)
  {
    rowsum[i] = 0;
    colsum[i] = 0;

    if(i == 0)
    {
      diasum1 = 1;
      diasum2 = 1;
      dia_label1 = sqmat.at<int>(i,i);     //First element of left diagonal
      dia_label2 = sqmat.at<int>(i,D-1-i); // First element of right diagonal
    }
    else
    {
      if(sqmat.at<int>(i,i) == sqmat.at<int>(i-1,i-1))
        diasum1++;
      if(sqmat.at<int>(i,D-1-i) == sqmat.at<int>(i-1,D-1-i+1))
        diasum2++;
    }


    for(int j = 0; j < D; j++)
    {
      if(j == 0)
      {
        rowsum[i] = 1;
        colsum[i] = 1;
        row_label[i] = sqmat.at<int>(i, j);
        col_label[i] = sqmat.at<int>(j, i);
      }
      else
      {
        if(sqmat.at<int>(i, j) == sqmat.at<int>(i, j-1))
          rowsum[i] = rowsum[i] + 1;
        if(sqmat.at<int>(j, i) == sqmat.at<int>(j-1, i))
          colsum[i] = colsum[i] + 1;
      }
    }
  }
  //---------------


  if((diasum1 == D) && (dia_label1 == playerA))
      winner = playerA;
  else if((diasum1 == D) && (dia_label1 == playerB))
      winner = playerB;
  else if((diasum2 == D) && (dia_label2 == playerA) )
      winner = playerA;
  else if((diasum2 == D) && (dia_label2 == playerB) )
      winner = playerB;
  else
  {
      bool nop_Flag = false; //noplayer flag
      int cntD = 0;
      for(int i = 0; i < D; i++)
      {
          if( (rowsum[i] == D) && (row_label[i] == playerA) )
          {
              winner = playerA;
              nop_Flag = false;
              break;
          }
          else if( (rowsum[i] == D) && (row_label[i] == playerB) )
          {
              winner = playerB;
              nop_Flag = false;
              break;
          }
          else if( (colsum[i] == D) && (col_label[i] == playerA) )
          {
              winner = playerA;
              nop_Flag = false;
              break;
          }
          else if( (colsum[i] == D) && (col_label[i] == playerB) )
          {
              winner = playerB;
              nop_Flag = false;
              break;
          }
          else
          {   
              nop_Flag = true;
              cntD++;
          }
      }

      if(nop_Flag == true && cntD == D)
          winner = noplayer;



      // If it is a draw if there are no empty spaces
      // and no player is a winner

      int nempty_cnt = 0;
      for(int i = 0; i < D; i++)
      {
          for(int j = 0; j < D; j++)
          {
              if(sqmat.at<int>(i,j) > 0)
                  nempty_cnt++;
          }
      }

      if(nempty_cnt == NS && winner == noplayer)
          drawFlag = true;
      else
          drawFlag = false;

#ifdef CHKWIN_DEBUG
    cout << "diasum1 = " << diasum1 << endl;
    cout << "diasum2 = " << diasum2 << endl;
    cout << "rowsum = " << rowsum[0] << "\t" << rowsum[1] << "\t" << rowsum[2] << endl;
    cout << "colsum = " << colsum[0] << "\t" << colsum[1] << "\t" << colsum[2] << endl;
    cout << "rowlabel = " << row_label[0] << "\t" << row_label[1] << "\t" << row_label[2] << endl;
    cout << "collabel = " << col_label[0] << "\t" << col_label[1] << "\t" << col_label[2] << endl;
    cout << "Winner = " << winner << endl;
    cout << "playerA =" << playerA << endl;
    cout << "playerB = " << playerB << endl;
    cout << "non-empty count =" << nempty_cnt << endl;

    cout << sqmat << endl;
    //getchar();

#endif


  } //else


    return(winner);

}
//========================================================
void ttt_game::make_move_random()
{
    next_state.copyTo(current_state); // preserve the last value

    cv::Mat eloc; // empty cell locations
    cv::Mat neloc; // non-empty cell locations
    find_empty_cell_locations(current_state, eloc, neloc);

#ifdef MVRND_DEBUG
    for(int i = 0; i < eloc.rows; i++)
        cout << eloc.at<int>(i) << "\t";
    cout << endl;
    cout << "eloc size = " << eloc.rows << "x" << eloc.cols << endl;
    cout << "press enter to continue" << endl;
    getchar();
#endif

    if(eloc.rows > 0)
    {
        // Which player makes the move
        player = (player%2)? 1:2;

        int choice = rng.uniform(0, eloc.rows);

        if(eloc.at<int>(choice) >= 0 && eloc.at<int>(choice) < NS)
        {
            if(player == playerA)
                next_state.at<int>(eloc.at<int>(choice)) = playerA;
            else
                next_state.at<int>(eloc.at<int>(choice)) = playerB;
        }

        player++;


        //-----------------------------

        // Add this state to State Table


        if(stateTable.rows == 0)
        {
            double v[2] = {0.5, 0.5};
            cv::Mat values= cv::Mat(1, 2, CV_64F, &v);

            stateTable.push_back(next_state);
            valueTable.push_back(values);

            cout << "values = " << values << endl;
            getchar();
        }
        else  // Table contains other states
        {
            int simIndex = find_state_index(next_state);


#ifdef MVRND_DEBUG
            cout << "size of state = " << next_state.rows<< "x" << next_state.cols << endl;
            cout << "State Table Size = " << stateTable.rows << "x" << stateTable.cols << endl;
            cout << "Value Table Size = " << valueTable.rows << "x" << valueTable.cols << endl;
#endif

            // if this is new state, add it to the table
            if(simIndex == -1)
            {
                double v[2] = {0.5, 0.5};
                cv::Mat values= cv::Mat(1, 2, CV_64F, &v);

                stateTable.push_back(next_state);
                valueTable.push_back(values);
            }
        }
    }
    else  // if there are no empty cells
    {
        // do nothing ...
    }
}
//===========================================
// Make a move
// =======================================================
bool ttt_game::make_move(cv::Mat &statevec)
{
    next_state.copyTo(statevec);

#ifdef DEBUG
    cout << __LINE__ << ": size of state = " << next_state.rows<< "x" << next_state.cols << endl;
    cout << __LINE__ << ": size of state = " << current_state.rows<< "x" << current_state.cols << endl;
    getchar();
#endif

    bool repeatFlag = false;

    player = (player%2)? 1:2;

    //cout << "Player = " << player << endl;

    int choice = (int)(rng.uniform(0.0,1.5)*NS);

    if(choice >= 0 && choice < NS)
    {
      if(next_state.at<int>(choice) == noplayer)
      {
        if(player == playerA)
          next_state.at<int>(choice) = playerA;
        else if(player == playerB)
          next_state.at<int>(choice) = playerB;
        else
          next_state.at<int>(choice) = noplayer;
      }
      else
      {

#ifdef DEBUG
          cout << "Can't alter the last value. Choose a new cell number" << endl;
#endif
          repeatFlag = true;
          player--;
      }
    }
    else
    {
#ifdef DEBUG
        std::cout << "Invalid choice for the cell. Choose a value between 0 and 8." << endl;
#endif
        repeatFlag = true;
        player--;
    }


    next_state.copyTo(statevec);

    // Add this state to State Table

    if(stateTable.rows == 0)
    {
        double v[2] = {0.5, 0.5};
        cv::Mat values= cv::Mat(1, 2, CV_64F, &v);

        stateTable.push_back(next_state);
        valueTable.push_back(values);
    }
    else  // Table contains other states
    {
        int simIndex = find_state_index(next_state);

        if(cv::norm(next_state) > 10.0)
        {
            cout << next_state << endl;
            getchar();
        }

#ifdef DEBUG
        cout << "size of state = " << next_state.rows<< "x" << next_state.cols << endl;
        cout << "State Table Size = " << stateTable.rows << "x" << stateTable.cols << endl;
        cout << "Value Table Size = " << valueTable.rows << "x" << valueTable.cols << endl;
#endif

        // if this is new state, add it to the table
        if(simIndex == -1)
        {
            double v[2] = {0.5, 0.5};
            cv::Mat values= cv::Mat(1, 2, CV_64F, &v);

            stateTable.push_back(next_state);
            valueTable.push_back(values);
        }
        else
        {

        }
    }


    player++;

    return(repeatFlag);
}
// ======================================
// compute the empty cell locations
// Input: current state vector
// Output: Array of indices
//==========================================
void ttt_game::find_empty_cell_locations(const cv::Mat& statevec, cv::Mat& eloc, cv::Mat& neloc)
{

    for(int i = 0; i < NS; i++)
    {
        if(statevec.at<int>(i) != noplayer )
            neloc.push_back(i);
        else
            eloc.push_back(i);
    }
}

//================================================================
// In self play:
// Player 1: Robot which learns while playing against a dumb oppont (player 2)
// Player 2: Dumb  opponent which makes random choices
// Robot updates the value function using temporal difference method
// Input: alpha - differential learning rate ( < 1.0)
//----------------------------------------------------------------

void ttt_game::make_move_selfplay(int pl, double alpha)
{
    next_state.copyTo(current_state); // preserve the last value

    int selfPlayer, dumbPlayer;
    if(pl == 1)
    {
        selfPlayer = playerA;
        dumbPlayer = playerB;
    }
    else if (pl == 2)
    {
        selfPlayer = playerB;
        dumbPlayer = playerA;
    }
    else
    {
        cout << __FILE__ << ": " << __func__<< ":" << __LINE__
             << "Invalid choice for first argument. Choose 1 or 2 as input" << endl;
        exit(-1);
    }

    cv::Mat eloc; // empty cell locations
    cv::Mat neloc; // non-empty cell locations

    find_empty_cell_locations(current_state, eloc, neloc);

#ifdef MVSP_DEBUG
    cout << "current state = " << current_state << endl;
    cout << "empty locations = " << eloc << endl;
    cout << "non-empty locations = " << neloc << endl;
    getchar();
#endif

    // A move is made only when there are empty
    // cells to be filled.

    if(eloc.rows > 0)
    {
        player = (player%2)? 1:2;

#ifdef MVSP_DEBUG

        cout << "player = " << player << endl;
        cout << "selfPlayer = " << selfPlayer << endl;
        cout << "dumbPlayer = " << dumbPlayer << endl;
        getchar();
#endif

        if(player == selfPlayer) // if it is the robot
        {
            cv::Mat child_idx_array;
            int matching_state_idx;

            //double pdf = boltzman_pdf_policy_selection(child_idx_array, matching_state_idx);

            matching_state_idx = compute_children(child_idx_array);

            double pdf = rng.uniform(0.0,1.0);

#ifdef MVSP_DEBUG
            cout << "pdf = " << pdf << endl;
            cout << "child_idx_array = " << child_idx_array << endl;
            cout << "matching idx = " << matching_state_idx << endl;
            cout << "Child of the current state" << endl;
            for(int i = 0; i< child_idx_array.rows; i++)
                cout << child_idx_array.at<int>(i) << "\t"
                     << stateTable.row(child_idx_array.at<int>(i))
                     << "\t Value = " << valueTable.row(child_idx_array.at<int>(i)) << endl;
            getchar();
#endif

            if(pdf > 0.1 ) // greedy move (exploitation)
            {
                double maxValue = -1.0;
                int maxValue_idx = -1;
                int maxChild_idx = -1;

                double tempValue = -1.0;
                int temp_idx = 0;
                int tempChild_idx = 0;
                int diff_cell_location = -1;


                for(int j = 0; j < child_idx_array.rows; j++)
                {
                    if(valueTable.at<double>(child_idx_array.at<int>(j), selfPlayer-1) >= tempValue)
                    {
                        tempValue = valueTable.at<double>(child_idx_array.at<int>(j), selfPlayer-1);
                        temp_idx = child_idx_array.at<int>(j);
                        tempChild_idx = j;

                        int simcnt = 0;
                        for(int i = 0; i < NS; i++)
                        {
                            if(current_state.at<int>(i) == stateTable.at<int>(temp_idx, i))
                                simcnt++;
                            else
                                diff_cell_location = i;
                        }

                        if(diff_cell_location != -1)
                        {
                            if(stateTable.at<int>(temp_idx, diff_cell_location) == selfPlayer)
                            {
                                maxValue_idx = temp_idx;
                                maxChild_idx = tempChild_idx;
                                maxValue = tempValue;
                            }
                            else
                            {
                                tempValue = -1.0;
                            }
                        }
                    }
                }

#ifdef MVSP_DEBUG
                cout << "max value = " << maxValue << ":  maxvalue ID = " << maxValue_idx << endl;
                cout << "diff_cell_location = " << diff_cell_location << endl;
                getchar();
#endif

                if(maxValue_idx != -1) // If there is a valid maxValue ID
                {
                    stateTable.row(maxValue_idx).copyTo(next_state);

                    // update the value for this state (differential learning)
                    // V(s) = V(s) + alpha * [V(s') - V(s)]
                    // s : state before greedy move
                    // s' : state after greedy move

                    valueTable.at<double>(matching_state_idx, selfPlayer-1) = valueTable.at<double>(matching_state_idx, selfPlayer-1)
                            + alpha * ( valueTable.at<double>(maxValue_idx, selfPlayer-1) - valueTable.at<double>(matching_state_idx, selfPlayer-1) );
                }
                else // if there is no child with player 1 move, make a random move for robot
                {
                    int choice = rng.uniform(0, eloc.rows);

                    if(eloc.at<int>(choice) >= 0 && eloc.at<int>(choice) < NS)
                        next_state.at<int>(eloc.at<int>(choice)) = selfPlayer;

                }
            }
            else  // exploration (choose a random state)
            {
                //cout << "Exploration - random move for robot" << endl;
                int choice = rng.uniform(0, eloc.rows);

                if(eloc.at<int>(choice) >= 0 && eloc.at<int>(choice) < NS)
                    next_state.at<int>(eloc.at<int>(choice)) = selfPlayer;

            }
        }
        else if(player == dumbPlayer) // the dumb opponent
        {
            int choice = rng.uniform(0, eloc.rows);

            if(eloc.at<int>(choice) >= 0 && eloc.at<int>(choice) < NS)
                next_state.at<int>(eloc.at<int>(choice)) = dumbPlayer;

        }

        // If the next_state is a new one, add it to the state value table
        int simIndex = find_state_index(next_state);
        if(simIndex == -1)
        {
            double v[2] = {0.5, 0.5};
            cv::Mat values = cv::Mat(1, 2, CV_64F, &v);

            stateTable.push_back(next_state);
            valueTable.push_back(values);
        }

#ifdef MVSP_DEBUG
        cout << "Matching State Index = " << simIndex << endl;
        cout << "Next State = " << next_state << endl;
        getchar();
#endif


        player++;
    }
    else
    {
        // if there are no empty locations.
        // do nothing
    }

}
//======================================================
// Player 1 is the robot
// Robot's greedy move - takes the next step which yields maximum value
// ------------------------------------------------------------------------
void ttt_game::make_move_robot(int selfPlayer, double alpha)
{
    next_state.copyTo(current_state); // preserve the last value

    cv::Mat eloc; // empty cell locations
    cv::Mat neloc; // non-empty cell locations

    find_empty_cell_locations(current_state, eloc, neloc);


    if(eloc.rows > 0)
    {
        cv::Mat child_idx_array;
        int matching_state_idx;

        //double pdf = boltzman_pdf_policy_selection(child_idx_array, matching_state_idx);

        matching_state_idx = compute_children(child_idx_array);

        // All children differ from the current state by only one cell location.


        double pdf = rng.uniform(0.0,1.0);

#ifdef MVROB_DEBUG
        cout << "self Player = " << selfPlayer << endl;
        cout << "pdf = " << pdf << endl;
        cout << "child_idx_array = " << child_idx_array << endl;
        cout << "current_state = " << current_state << endl;
        for(int i = 0; i< child_idx_array.rows; i++)
            cout << child_idx_array.at<int>(i) << "\t" << stateTable.row(child_idx_array.at<int>(i))
                 << "\t Value = " << valueTable.row(child_idx_array.at<int>(i)) << endl;
        getchar();
#endif

        if(pdf > 0.1) // greedy move - exploitation
        {
            double maxValue = -1.0;
            int maxValue_idx = -1;
            int maxChild_idx = -1;

            double tempValue = -1.0;
            int temp_idx = 0;
            int tempChild_idx = 0;
            int diff_cell_location = -1;


            for(int j = 0; j < child_idx_array.rows; j++)
            {
                if(valueTable.at<double>(child_idx_array.at<int>(j), selfPlayer-1) >= tempValue)
                {
                    tempValue = valueTable.at<double>(child_idx_array.at<int>(j), selfPlayer-1);
                    temp_idx = child_idx_array.at<int>(j);  // actual row Index in stateTable
                    tempChild_idx = j;


                    int simcnt = 0;
                    for(int i = 0; i < NS; i++)
                    {
                        if(current_state.at<int>(i) == stateTable.at<int>(temp_idx, i))
                            simcnt++;
                        else
                            diff_cell_location = i;
                    }

                    if(diff_cell_location != -1)
                    {
                        if(stateTable.at<int>(temp_idx, diff_cell_location) == selfPlayer)
                        {
                            maxValue_idx = temp_idx;
                            maxChild_idx = tempChild_idx;
                            maxValue = tempValue;
                        }
                        else // reset tempValue
                        {
                            tempValue = -1.0;
                        }
                    }
                    else
                    {
                        cout << __FILE__ << ": " << __LINE__ << ": "
                             << __func__ << ": This should not happen. Check again" << endl;
                        exit(-1);
                    }
                }
            }


#ifdef MVROB_DEBUG
            cout << "Exploitation - Greedy Move" << endl;
            cout << "selfPlayer = " << selfPlayer << endl;
            cout << "diff_cell_location = " << diff_cell_location << endl;
            cout << "maxValue_idx = " << maxValue_idx << " : maxValue = " << maxValue << endl;
            getchar();
#endif

            if(maxValue_idx != -1)
            {
                stateTable.row(maxValue_idx).copyTo(next_state);

                valueTable.at<double>(matching_state_idx, selfPlayer-1) = valueTable.at<double>(matching_state_idx, selfPlayer-1)
                        + alpha * ( valueTable.at<double>(maxValue_idx, selfPlayer-1) - valueTable.at<double>(matching_state_idx, selfPlayer-1) );
            }
            else // make a random move for robot
            {
                int choice = rng.uniform(0, eloc.rows);

                if(eloc.at<int>(choice) >= 0 && eloc.at<int>(choice) < NS)
                    next_state.at<int>(eloc.at<int>(choice)) = selfPlayer;
            }
        }
        else   // Exploration
        {

            //cout << "Exploration - random move for robot" << endl;
            int choice = rng.uniform(0, eloc.rows);

            if(eloc.at<int>(choice) >= 0 && eloc.at<int>(choice) < NS)
                next_state.at<int>(eloc.at<int>(choice)) = selfPlayer;

#ifdef MVROB_DEBUG
            cout << "Exploration - Random Move" << endl;
#endif

        }

        // Check if this is a new state, if yes, store it in the table.

        int simIndex = find_state_index(next_state);
        if(simIndex == -1)
        {
            double v[2] = {0.5, 0.5};
            cv::Mat values = cv::Mat(1, 2, CV_64F, &v);

            stateTable.push_back(next_state);
            valueTable.push_back(values);
        }

#ifdef MVROB_DEBUG
        cout << "Next State = " << next_state << endl;
        cout << "state matching index = " << simIndex << endl;
        getchar();
#endif

    }
    else
    {
        // there are no empty cells.
        // nothing to do ...
    }
}
//==========================================
void ttt_game::make_move_human(int dumbPlayer)
{
    next_state.copyTo(current_state); // preserve the last value

    cv::Mat eloc; // empty cell locations
    cv::Mat neloc; // non-empty cell locations

    find_empty_cell_locations(current_state, eloc, neloc);

    cout << "Empty Cell Locations: " << eloc << endl;


    if(eloc.rows > 0)
    {
        bool valid = false;
        int choice;
        do
        {
            cout << "Enter your choice for cell locations" << endl;
            cin >> choice;


            for(int i = 0; i < eloc.rows; i++)
            {
                if(choice == eloc.at<int>(i))
                {
                    valid = true;
                    break;
                }
            }
        }while(!valid);

        next_state.at<int>(choice) = dumbPlayer; // human

        //cout << "next_state = " << next_state << endl;
        //getchar();
    }
}

//============================================
// Compute the value function for all afterstates
// Afterstates are the possible states resulting from current state
//----------------------------------------
double ttt_game::boltzman_pdf_policy_selection(int pl, cv::Mat& child_idx_array, int& matching_state_idx)
{
    cv::Mat eloc; // empty cell locations
    cv::Mat neloc; // non-empty cell locations
    find_empty_cell_locations(current_state, eloc, neloc);


#ifdef DEBUG
    cout << "eloc size = " << eloc.rows << "x" << eloc.cols << endl;
    cout << "neloc size = " << neloc.rows << "x" << neloc.cols << endl;
    getchar();
#endif

    double T = 100.0; // temperature constant
    double sum = 0.0;

    matching_state_idx = -1; // initial value is -1
    for(int i = 0; i < stateTable.rows; i++)
    {
        int ne_match_cnt = 0;
        int empty_match_cnt = 0;
        int simcnt = 0;


        for(int j = 0; j < NS; j++)
        {
            for(int k = 0; k < neloc.rows; k++)
            {
                if(j == neloc.at<int>(k))
                {
                    if(current_state.at<int>(j) == stateTable.at<int>(i,j))
                        ne_match_cnt++;
                }
            }

            for(int k = 0; k < eloc.rows; k++)
            {
                if(j == eloc.at<int>(k))
                {
                    if(current_state.at<int>(j) == stateTable.at<int>(i,j))
                        empty_match_cnt++;
                }
            }

            if(current_state.at<int>(j) == stateTable.at<int>(i,j))
            {
                simcnt++;
            }

        }

        //only one of the empty locations should be filled
        if(ne_match_cnt == neloc.rows && empty_match_cnt == (eloc.rows - 1))
        {
            child_idx_array.push_back(i);
            sum += exp(valueTable.at<double>(i, pl-1) / T);
        }

        if(simcnt == NS)
            matching_state_idx = i;
    }

    double pdf;
    if(matching_state_idx >= 0 && matching_state_idx < stateTable.rows )
    {
        pdf = exp(valueTable.at<double>(matching_state_idx, pl-1) / T) / sum;
    }
    else
    {
        pdf = exp(0.5/T) / sum;
    }

    //probability
    if(pdf > 1.0)
        pdf = 1.0;
    else if(pdf < 0.0)
        pdf = 0.0;

#ifdef DEBUG
    cout << "matching state idx = " << matching_state_idx << endl;
    cout << __LINE__<< ":"<<  __func__ << "\t: pdf = " << pdf << endl;
    getchar();
#endif

    return(pdf);
}

//========================================================================================
// Returns the children of a given state
int ttt_game::compute_children(cv::Mat& child_idx_array)
{
    cv::Mat eloc; // empty cell locations
    cv::Mat neloc; // non-empty cell locations

    find_empty_cell_locations(current_state, eloc, neloc);

    int matching_state_idx = -1; // initial value is -1


    for(int i = 0; i < stateTable.rows; i++)
    {
        int ne_match_cnt = 0;
        int empty_match_cnt = 0;
        int simcnt = 0;


        for(int j = 0; j < NS; j++)
        {
            for(int k = 0; k < neloc.rows; k++)
            {
                if(j == neloc.at<int>(k))
                {
                    if(current_state.at<int>(j) == stateTable.at<int>(i,j))
                        ne_match_cnt++;
                }
            }

            for(int k = 0; k < eloc.rows; k++)
            {
                if(j == eloc.at<int>(k))
                {
                    if(current_state.at<int>(j) == stateTable.at<int>(i,j))
                        empty_match_cnt++;
                }
            }

            if(current_state.at<int>(j) == stateTable.at<int>(i,j))
            {
                simcnt++;
            }
        }

        //only one of the empty locations should be filled
        if(ne_match_cnt == neloc.rows && empty_match_cnt == (eloc.rows - 1))
            child_idx_array.push_back(i);

        if(simcnt == NS)
            matching_state_idx = i;
    }
    return matching_state_idx;
}

//============================================

void ttt_game::update_valueTable(bool drawF)
{
    winIndex = find_state_index(next_state);

    if(winner == playerA) //agent wins
    {
        valueTable.at<double>(winIndex,0) = 1.0;
        valueTable.at<double>(winIndex,1) = 0.0;
    }
    else if (winner == playerB)
    {
        valueTable.at<double>(winIndex, 0) = 0.0;
        valueTable.at<double>(winIndex, 1) = 1.0;
    }
    else if(winner == noplayer && drawF == true)
    {
        valueTable.at<double>(winIndex, 0) = 0.0;
        valueTable.at<double>(winIndex, 1) = 0.0;
    }
    else
    {
        valueTable.at<double>(winIndex, 0) = 0.5;
        valueTable.at<double>(winIndex, 1) = 0.5;
    }
}

//===============================================
int ttt_game::find_state_index(const cv::Mat &statevec)
{
    bool simFlag = false;
    int idx = -1;  // no match found

    for(int k = 0; k < stateTable.rows; k++)
    {
        int simcnt = 0;
        for(int i = 0; i < NS; i++)
        {
            if(statevec.at<int>(i) == stateTable.at<int>(k,i))
                simcnt++;
        }
        if(simcnt == NS)
        {
            simFlag = true;
            idx = k;
            break;
        }
    }

   return(idx);
}

//======================================================
// Print State table

int ttt_game::print_stateValueTable(const char *filename)
{
     ofstream f1(filename);

     if(f1)
     {
         for(int k = 0; k < stateTable.rows; k++)
         {
             for(int j = 0; j < NS; j++)
                 f1 << stateTable.at<int>(k,j) << "\t";
             f1 << valueTable.at<double>(k, 0) << "\t"
                << valueTable.at<double>(k,1) << endl;
         }

         f1.close();
     }
     else
     {
         cerr << "File does not exist ..." << endl;
         exit(-1);
     }

     cout << "Number of rows = " << stateTable.rows << endl;
    return(stateTable.rows);
}
//======================================================
// read the state table

int ttt_game::read_stateValueTable(const char *filename, int ndata)
{
   stateTable = cv::Mat::zeros(ndata, NS, CV_32SC1);
   valueTable = cv::Mat::zeros(ndata, 2, CV_64F);

    ifstream f1(filename);

    if(f1.good())
    {
        int a;
        double b, c;

        for(int k = 0; k < ndata; k++)
        {
            for(int j = 0; j < NS; j++)
            {
                f1 >> a;
                stateTable.at<int>(k,j) = a;
            }

            f1 >> b >> c;
            valueTable.at<double>(k,0) = b;
            valueTable.at<double>(k,1) = c;
        }
        f1.close();

#ifdef DEBUG
        for(int k = 0; k < ndata; k++)
        {
            for(int j = 0; j < NS; j++)
                cout << stateTable.at<int>(k,j) << "\t";
            cout << valueTable.at<double>(k, 0) << "\t"
                 << valueTable.at<double>(k, 1) << endl;
        }
        getchar();
#endif
    }
    else
    {
        cerr << "File does not exist ..." << endl;
        exit(-1);
    }
}

//=======================================
int ttt_game::get_board_dimension()
{
    return(D);
}
//===================================
void ttt_game::reset_board()
{
    current_state = cv::Mat::zeros(1, NS, CV_32SC1);
    next_state = cv::Mat::zeros(1, NS, CV_32SC1);
}
//==============================================================
bool ttt_game::check_gameover(const cv::Mat &statevec)
{
    int cnt = 0;
    for(int i = 0; i < NS; i++)
    {
        if(statevec.at<int>(i) > 0)
            cnt++;
    }
    if(cnt == NS)
        return true;
    else
        return false;
}
//=============================================================
void ttt_game::display_stateValueTable()
{

    for(int k = 0; k < stateTable.rows; k++)
    {
        cout << k << ": " << stateTable.row(k) << ": " << valueTable.row(k) << endl;
    }

}
//====================================================================
void ttt_game::set_next_state(const cv::Mat& statevec)
{
#ifdef DEBUG
    cout << "statevec size = " << statevec.size() << endl;
    cout << "next_state size = " << next_state.size() << endl;
    getchar();
#endif

    if(statevec.size() == next_state.size() ) //statevec.cols == next_state.cols)
    {
        statevec.copyTo(next_state);
    }
}

//========================================
void ttt_game::get_next_state(cv::Mat& statevec)
{
#ifdef GNS_DEBUG
    cout << "statevec size = " << statevec.size() << endl;
    cout << "next_state size = " << next_state.size() << endl;
    getchar();
#endif

    if(statevec.size() == next_state.size() ) //statevec.cols == next_state.cols)
    {
        next_state.copyTo(statevec);
    }

}

