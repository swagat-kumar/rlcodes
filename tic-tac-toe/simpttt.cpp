/* A Simple Tic-Tac-Toe Game 
 *
 * Grid can be of any dimension. 
 *
 * Date: June 02, 2016
 * Author: Swagat Kumar (swagat.kumar@gmail.com)
 * License: GPL V 3.0 or Higher
 * --------------------------- */

#include <iostream>
#include <fstream>
#include <cmath>

using namespace std;

const int D = 3;
void display_board(int sqmat[][D]);
int check_winner(int sqmat[][D]);
int check_winner2(int sqvec[]);

const  int playerA = 1;
const  int playerB = 2;
const  int noplayer = 0;


int main()
{
  const int Ns = D*D;

  int sqVec[Ns];
  int sqMat[D][D];

  for(int i = 0; i < Ns; i++)
    sqVec[i] = 0;

  int k = 0;
  for(int i = 0; i < D; i++)
    for(int j = 0; j < D; j++)
    {
      sqMat[i][j] = sqVec[k]; 
      k++;
    }

  int player = 1;
  int choice = 0;
  int winner = 0;
  int no_moves = 0;

  do
  {
    display_board(sqMat);

    int player = (player%2)? 1:2; 

    std::cout << "Player " << player << " : Select the cell to mark between 0 and " <<  Ns-1 << endl; 
    cin >> choice;

    if(choice >= 0 && choice < Ns)
    {
      if(sqVec[choice] == noplayer)
      {
        if(player == playerA)
          sqVec[choice] = playerA;
        else if(player == playerB)
          sqVec[choice] = playerB;
        else
          sqVec[choice] = noplayer;
      }
      else
      {
        cout << "Can't alter the last value. Choose a new cell number" << endl;
        continue;  // Skip the rest of the do-while loop
      }
    }
    else
    {
      std::cout << "Invalid choice for the cell. Choose a value between 0 and 8." << endl;
      continue; // skip the rest of the do-while loop
    }


    k = 0;
    for(int i = 0; i < D; i++)
      for(int j = 0; j < D; j++)
      {
        sqMat[i][j] = sqVec[k]; 
        k++;
      }

    // Check the winner

    winner = check_winner(sqMat);


    if(winner != 0)
    {
      cout << "The winner is player: " << winner << endl;
      break;
    }

    // turn for the next player to make a move
    player++;
    no_moves++;
  }while(no_moves < Ns); 

  cout << "Final Board status" << endl;
  display_board(sqMat);

  if(winner == 0 && no_moves >= Ns)
    cout << "No one wins" << endl;


  return 0;
}
//==============================================

int check_winner(int sqmat[][D])
{
  int rowsum[D];
  int colsum[D];
  int diasum1 = 0;
  int diasum2 = 0;
  int row_label[D];
  int col_label[D];
  int dia_label1;
  int dia_label2;

  for(int i = 0; i < D; i++)
  {
    rowsum[i] = 0;
    colsum[i] = 0;

    if(i == 0)
    {
      diasum1 = 1;
      diasum2 = 1;
      dia_label1 = sqmat[i][i];
      dia_label2 = sqmat[i][D-1-i];
    }
    else
    {
      if(sqmat[i][i] == sqmat[i-1][i-1])
        diasum1++;
      if(sqmat[i][D-1-i] == sqmat[i-1][D-1-i+1])
        diasum2++;
    }

    for(int j = 0; j < D; j++)
    {
      if(j == 0)
      {
        rowsum[i] = 1;
        colsum[i] = 1;
        row_label[i] = sqmat[i][j];
        col_label[i] = sqmat[j][i];
      }
      else
      {
        if(sqmat[i][j] == sqmat[i][j-1])
          rowsum[i] = rowsum[i] + 1;
        if(sqmat[j][i] == sqmat[j-1][i])
          colsum[i] = colsum[i] + 1;
      }
    }
  }
  //---------------


  if((diasum1 == D) && (dia_label1 == playerA))
    return(playerA);
  else if((diasum1 == D) && (dia_label1 == playerB))
    return(playerB);
  else if((diasum2 == D) && (dia_label2 == playerA) )                                                            
    return(playerA);
  else if((diasum2 == D) && (dia_label2 == playerB) )                                                            
    return(playerB);
  else
  {
    bool draw = false;
    for(int i = 0; i < D; i++)
    {
      if( (rowsum[i] == D) && (row_label[i] == playerA) )
        return(playerA);
      else if( (rowsum[i] == D) && (row_label[i] == playerB) )
        return(playerB);
      if( (colsum[i] == D) && (col_label[i] == playerA) )
        return(playerA);
      else if( (colsum[i] == D) && (col_label[i] == playerB) )
        return(playerB);
      else
        draw = true;
    }

    if(draw == true)
      return(noplayer);
  }
}
//---------------------------------------


void display_board(int sqmat[][D])
{
  cout << "--------------------------------" << endl;
  for(int i = 0; i < D; i++)
  {
    for(int j = 0; j < D; j++)
     {
       if(sqmat[i][j] == playerA)
         cout << 'X' << "\t";
       else if(sqmat[i][j] == playerB)
         cout << 'O' << "\t";
       else
         cout << "--" << "\t";
     }
    cout << endl;
  }
  cout << "-----------------------------------" << endl;
}

