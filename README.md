Project Title: RL Codes

Purpose: Aims to provide various reinforcement learning codes in C/C++

Author: Swagat Kumar

Email: swagat.kumar@gmail.com

License: GPL V 2.0 or above

-----------------------------

Dependencies:
-------------------
- GNU/Linux (Ubuntu 14.04)
- G++ 


Updates: 
-------------
Date: August 06, 2016

- Tic-Tac-Toe Game
   - A generic tic-tac-toe game is available: "simpttt.cpp"
   - Data Generation Module is ready: "test.cpp"
   - Binary / Ternary search tree programs: trees/tst/, trees/bst/




